package org.company.entities;

public class Person {
    private String patronymic;

    private String name;

    private String surname;
    private int age;
    private String gender;
    private int phoneNum;
    public Person(String patronymic,String name,String surname,int age,String gender, int phoneNum){
       this.name = name;
       this.surname = surname;
       this.patronymic = patronymic;
       this.age = age;
       this.gender = gender;
       this.phoneNum = phoneNum;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPns() {
        return patronymic;
    }

    public void setPns(String pns) {
        this.patronymic = patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }

    @Override
    public String toString() {
        return "Person{" +
                "patronymic='" + patronymic + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", phoneNum=" + phoneNum +
                '}';
    }
}
