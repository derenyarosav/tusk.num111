package org.company.vehicles.entities;

public class Lorry extends Car {


    private int carryingCapacityOfTheBody;

    public Lorry(String carBrand, String carMark, int wight, int carryingCapacityOfTheBody) {
        super(carBrand, carMark, wight);
        this.carryingCapacityOfTheBody = carryingCapacityOfTheBody;

    }


    public int getCarryingCapacityOfTheBody() {
        return carryingCapacityOfTheBody;
    }


    public void setCarryingCapacityOfTheBody(int carryingCapacityOfTheBody) {
        carryingCapacityOfTheBody = carryingCapacityOfTheBody;
    }

    @Override
    public void start() {
        System.out.println("Починаю таягнути вантаж");
    }

    @Override
    public String toString() {
        return "Lorry{" +
                "carryingCapacityOfTheBody=" + carryingCapacityOfTheBody +
                '}';
    }
}

