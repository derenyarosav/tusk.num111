package org.company.vehicles.entities;

public class SportCar extends Car {
    private String maximumSpeed;

    public SportCar(String carBrand, String carMark, int wight, String maximumSpeed) {
        super(carBrand, carMark, wight);
        this.maximumSpeed = maximumSpeed;

    }

    public String getMaximumSpeed() {
        return maximumSpeed;
    }

    public void setMaximumSpeed(String maximumSpeed) {
        maximumSpeed = maximumSpeed;
    }

    @Override
    public void start() {
        System.out.println("Починаю швидко їхати ");


}

    @Override
    public String toString() {
        return "SportCar{" +
                "maximumSpeed='" + maximumSpeed + '\'' +
                '}';
    }
}
