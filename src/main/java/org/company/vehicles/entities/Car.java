package org.company.vehicles.entities;

import org.company.details.Engine;
import org.company.professions.Driver;

public class Car {
    private String carBrand;
    private String carMark;
    private int wight;

    public Car(String carBrand, String carMark, int wight) {
        this.carBrand = carBrand;
        this.carMark = carMark;
        this.wight = wight;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarMark() {
        return carMark;
    }

    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }

    public int getWight() {
        return wight;
    }

    public void setWight(int wight) {
        this.wight = wight;
    }

    public void start() {
        System.out.println("Поїхали");
    }

    public void stop() {
        System.out.println("Start");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");

    }

    @Override
    public String toString() {
        return "Car{" +
                "carBrand='" + carBrand + '\'' +
                ", carMark='" + carMark + '\'' +
                ", wight=" + wight +
                '}';
    }
}


